# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Documentation building
## [1.0.0] - 2021-10-03
### Added
- Logging functions for trivial logging in the LiteLog namespace
- Logging levels: TRACE, DEBUG, INFO, NOTICE, WARN, ERROR, FATAL

[Unreleased]: https://gitlab.com/Chris__/litelogcpp/-/compare/v1.0.0...master?from_project_id=29314838
[1.0.0]: https://gitlab.com/Chris__/litelogcpp/-/tags/v1.0.0