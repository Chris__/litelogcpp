/*
 * Copyright (c) 2021 Christian Nagel
 *
 * This software is licensed under the MIT license. You should have received a
 * copy of it as part of this distribution. If not, the license terms are also available
 * at https://opensource.org/licenses/mit-license.php.
 */

#include <litelogcpp/LiteLog.h>
#include <iomanip>
#include <iostream>

/*
  ANSI escape color codes:
  Name            FG
  Black           30
  Red             31
  Green           32
  Yellow          33
  Blue            34
  Magenta         35
  Cyan            36
  White           37
*/

// Trivial Logging Functions

template<> void LiteLog::log<LiteLog::TRACE>(const std::string& text)
{
    printLog("[TRACE] " + text, "36");
}
template<> void LiteLog::log<LiteLog::DEBUG>(const std::string& text)
{
    printLog("[DEBUG] " + text, "37");
}
template<> void LiteLog::log<LiteLog::INFO>(const std::string& text)
{
    printLog("[INFO] " + text, "34");
}
template<> void LiteLog::log<LiteLog::NOTICE>(const std::string& text)
{
    printLog("[NOTICE] " + text, "32");
}
template<> void LiteLog::log<LiteLog::WARN>(const std::string& text)
{
    printLog("[WARN] " + text, "33");
}
template<> void LiteLog::log<LiteLog::ERROR>(const std::string& text)
{
    printLog("[ERROR] " + text, "31");
}
template<> void LiteLog::log<LiteLog::FATAL>(const std::string& text)
{
    printLog("[FATAL] " + text, "91");
}
void LiteLog::printLog(const std::string& text, std::string_view colorCode)
{
    // To get the time
    std::time_t time = std::time(nullptr);
    // and get the corresponding local time
    std::tm localtime = *std::localtime(&time);

    std::stringstream msg;
    msg << "\x1B[" << colorCode << "m" << std::put_time(&localtime, "%d-%m-%Y %H:%M:%S") << " " << text << std::endl;
    // Print combined string out
    std::cout << msg.str();
}
