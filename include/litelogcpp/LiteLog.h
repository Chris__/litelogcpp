/*
 * Copyright (c) 2021 Christian Nagel
 *
 * This software is licensed under the MIT license. You should have received a
 * copy of it as part of this distribution. If not, the license terms are also available
 * at https://opensource.org/licenses/mit-license.php.
 */

#ifndef FIREAMPSERVER_LITELOG_H
#define FIREAMPSERVER_LITELOG_H

#include <string>

/**
 * @brief Namespace for the logging functions of LiteLog++.
 *
 * @details Contains the trivial logging functions of LiteLog++
 */
namespace LiteLog
{
    enum LOGGING_LEVEL
    {
        TRACE,  /*!< @brief Logging level for developers. @details Shouldn't be used in production */
        DEBUG, /*!< @brief Logging level for debug messages */
        INFO, /*!< @brief Logging level for infos also useful to users */
        NOTICE, /*!< @brief Logging level for infos also useful to users, but more significant than INFOs */
        WARN, /*!< @brief Logging level for warnings, for example unexpected or unusual situations @brief Often the application stops after that */
        ERROR, /*!< @brief Logging level for errors which the application can handle in some way  */
        FATAL /*!< @brief Logging level for fatal errors which are really bad @brief Often the application stops after that */
    };
    /**
     * @brief Prints a log message.
     *
     * @details Prints a log message tagged as specified in the @p level template parameter to the log in the following format: "DD-MM-YYYY hh:mm:ss [logging level] logging
     * message"
     * @tparam level the logging level of the log message
     * @param text to be printed
     *
     */
    template <LOGGING_LEVEL level> void log(const std::string& text);
    template <> void log<TRACE>(const std::string& text);
    template <> void log<DEBUG>(const std::string& text);
    template <> void log<INFO>(const std::string& text);
    template <> void log<NOTICE>(const std::string& text);
    template <> void log<WARN>(const std::string& text);
    template <> void log<ERROR>(const std::string& text);
    template <> void log<FATAL>(const std::string& text);

    /**
     * @brief Prints colored text to the log.
     *
     * @details Prints the given log message to the log in this format: "DD-MM-YYYY hh:mm:ss logging message"
     * The text will be colored in the given color.
     * @param text to be printed
     * @param colorCode color of text as ANSI Code
     */
    void printLog(const std::string& text, std::string_view colorCode);
} // namespace LiteLog

#endif // FIREAMPSERVER_LITELOG_H
