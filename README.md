# Minimal C++ Logging Library

## Overview

LiteLogCpp (or LiteLog++) is a minimally sized logging library. It is written in C++ and builds with CMake.
The project was started because many log libraries have too many features for small projects which only
need trivial logging functions or where minimum package size is needed.

## Using the library

Using the library is very simple. You have to link against the library in your project through you build system. Then
include the "LiteLog.h" header file and use the methods of the LiteLog namespace. For example:

    #include <litelogcpp/LiteLog.h>
    
    int main() {
        // This logs "Test" with the current date and time in cyan with the trace tag:
        LiteLog::log<LiteLog::TRACE>("Test");
        // Example Output: "07-09-2021 13:17:56 [TRACE] Test"
    }

There are the following functions in the LiteLog namespace:

    void log<LiteLog::TRACE>(const std::string& text);
    void log<LiteLog::DEBUG>(const std::string& text);
    void log<LiteLog::INFO>(const std::string& text);
    void log<LiteLog::NOTICE>(const std::string& text);
    void log<LiteLog::WARN>(const std::string& text);
    void log<LiteLog::ERROR>(const std::string& text);
    void log<LiteLog::FATAL>(const std::string& text);

    // All logging functions call this function in the end:
    void printLog(const std::string& text, std::string_view colorcode);

## Supported Platforms

The library is cross-platform and should be able to be built on all platforms which have a C++ compiler which is supported by CMake.

## Building

The project is built with CMake, so make sure you have version 3.19 or higher installed. To build,
execute this from the root directory (the one with the "src" folder)

    mkdir build-output
    cmake . -B build-output
    cd build-output
    make

This builds the library for your platform and will generate a shared library in the "build-output" directory.

## Binaries

For each commit binaries are built with GitLab CI as artifacts. These are only for the Linux x86-64 platform, but on release the library is built for all major plattforms (x86-64, x86, Arm, Arm64, )

## Bugreports and Feature requests

Please submit an issue to this repo if you find any bugs or have an idea for a feature. If the bug is security-relevant,
please mark the issue as confidential. For more information read the [Contribution Guidelines](CONTRIBUTING.md).

## Contributions

Contributions are welcome, and it would be amazing if you want to help. Refer to
the [Contribution Guidelines](CONTRIBUTING.md) for more information.

## Contributors

All the contributors will be listed in the THANKS file.

## License

LiteLog++ is licensed under the MIT license, which is a simple and straight to the point license. It allows you to
do almost anything as long as you include the license and copyright notice. You can read it [here](LICENSE).
